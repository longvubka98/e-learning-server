CREATE TABLE users(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    name            VARCHAR(255)     NOT NULL,
    email           VARCHAR(255)     NOT NULL      UNIQUE,
    avatar_url       TEXT,
    enabled         BOOLEAN          DEFAULT TRUE,
    password        VARCHAR(255),
    provider_type        VARCHAR(255)     NOT NULL,
    provider_id     VARCHAR(255),
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    expo_push_notification_key  VARCHAR(100)
);
CREATE INDEX users_email_index ON users(email);

CREATE TABLE posts(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    user_id         bigint not null,
    message            TEXT,
    post_type       VARCHAR(25),
    deleted          BOOLEAN          DEFAULT FALSE,
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP
);

CREATE TABLE post_images(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    post_id         bigint not null,
    picture           TEXT
);

CREATE TABLE exam(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    post_id         bigint not null,
    grade           VARCHAR(255),
    subjects        VARCHAR(255),
    semester        VARCHAR(255),
    total_questions int,
    duration        int
);

CREATE TABLE exam_answers(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    exam_id         bigint not null,
    question_index  smallint,
    answer_index    smallint
);

CREATE TABLE exam_results(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    user_id         bigint not null,
    exam_id         bigint not null,
    total_correct   int,
    total_wrong     int,
    started_at      TIMESTAMP,
    finished_at      TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE post_votes(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    post_id         bigint not null,
    user_id         bigint not null,
    vote_type        VARCHAR(25),
    voted_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE post_comments(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    post_id         bigint not null,
    user_id         bigint not null,
    message        TEXT,
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE groups(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    name            VARCHAR(255),
    descriptions VARCHAR(255),
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    created_by      bigint not null
);

CREATE TABLE group_members(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    group_id         bigint not null,
    member_id bigint not null,
    joined_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE group_posts(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    group_id         bigint not null,
    message            TEXT,
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP,
    created_by      bigint not null
);