CREATE TABLE notifications(
    id              SERIAL           NOT NULL      PRIMARY KEY,
    sender         bigint,
    receiver         bigint not null,
    title           varchar(255),
    message        TEXT,
    created_at      TIMESTAMP        DEFAULT CURRENT_TIMESTAMP
);