package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.ExamResultEntity;

import java.util.List;

@Repository
public interface ExamResultRepository extends JpaRepository<ExamResultEntity, Long> {

    List<ExamResultEntity> findAllByUser_Id(long userId);

    @Query(nativeQuery = true, value = "SELECT user_id as userId, SUM(total_correct) as total from exam_results GROUP BY user_id ORDER BY total DESC LIMIT 20")
    List<RankingProjection> findTopUser();
}
