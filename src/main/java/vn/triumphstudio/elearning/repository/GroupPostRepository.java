package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.GroupPostEntity;
import vn.triumphstudio.elearning.domain.entity.PostEntity;

import java.util.List;

@Repository
public interface GroupPostRepository extends JpaRepository<PostEntity, Long> {

    List<PostEntity> findAllByGroup_Id(long groupId);
}
