package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.GroupEntity;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, Long>, PagingAndSortingRepository<GroupEntity, Long> {
}
