package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.ExamEntity;
import vn.triumphstudio.elearning.domain.entity.ExerciseEntity;

@Repository
public interface ExerciseRepository extends JpaRepository<ExerciseEntity, Long> {
    ExerciseEntity findFirstByPost_Id(long id);
}
