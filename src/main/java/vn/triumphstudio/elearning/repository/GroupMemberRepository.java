package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.GroupMemberEntity;

import java.util.List;

@Repository
public interface GroupMemberRepository extends JpaRepository<GroupMemberEntity, Long> {
    List<GroupMemberEntity> findAllByGroup_Id(long groupId);

    List<GroupMemberEntity> findAllByMember_Id(long memberId);

    GroupMemberEntity findFirstByGroup_IdAndMember_Id(long groupId, long memberId);
}
