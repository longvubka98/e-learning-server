package vn.triumphstudio.elearning.repository;

public interface RankingProjection {
    Long getUserId();

    Integer getTotal();
}
