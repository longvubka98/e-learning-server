package vn.triumphstudio.elearning.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.PostEntity;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<PostEntity, Long>, PagingAndSortingRepository<PostEntity, Long> {
    List<PostEntity> findAllByUser_Id(long userId, Sort sort);

    PostEntity findFirstByIdAndUser_Id(long postId, long userId);
}
