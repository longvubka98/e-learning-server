package vn.triumphstudio.elearning.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.triumphstudio.elearning.domain.entity.PostVoteEntity;
import vn.triumphstudio.elearning.domain.enumration.VoteType;

@Repository
public interface PostVoteRepository extends JpaRepository<PostVoteEntity, Long> {
    PostVoteEntity findFirstByPost_IdAndUser_Id(long postId, long userId);

    int countByPost_IdAndVoteType(long postId, VoteType voteType);
}
