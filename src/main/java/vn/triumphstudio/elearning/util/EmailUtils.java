package vn.triumphstudio.elearning.util;


import vn.triumphstudio.elearning.domain.enumration.AuthProvider;

public final class EmailUtils {
    private EmailUtils() {
    }

    public static String generateDefaultEmail(AuthProvider authProvider, String providerId) {
        return String.format("e_learning_%s_%s@gmail.com", authProvider.name(), providerId);
    }
}
