package vn.triumphstudio.elearning.domain.response;

import org.springframework.beans.BeanUtils;
import vn.triumphstudio.elearning.domain.entity.GroupEntity;
import vn.triumphstudio.elearning.domain.entity.GroupMemberEntity;
import vn.triumphstudio.elearning.domain.entity.GroupPostEntity;
import vn.triumphstudio.elearning.domain.entity.PostEntity;

import java.util.List;

public class GroupInfo extends GroupEntity {

    private List<GroupMemberEntity> members;

    private List<PostEntity> posts;

    public GroupInfo(GroupEntity groupEntity) {
        BeanUtils.copyProperties(groupEntity, this);
    }

    public List<GroupMemberEntity> getMembers() {
        return members;
    }

    public void setMembers(List<GroupMemberEntity> members) {
        this.members = members;
    }

    public List<PostEntity> getPosts() {
        return posts;
    }

    public void setPosts(List<PostEntity> posts) {
        this.posts = posts;
    }
}
