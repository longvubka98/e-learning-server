package vn.triumphstudio.elearning.domain.response;

import vn.triumphstudio.elearning.domain.entity.UserEntity;

public class RankingResponse {

    private UserEntity user;

    private int totalCorrect;

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public int getTotalCorrect() {
        return totalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        this.totalCorrect = totalCorrect;
    }
}
