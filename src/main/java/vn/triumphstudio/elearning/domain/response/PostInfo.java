package vn.triumphstudio.elearning.domain.response;

import org.springframework.beans.BeanUtils;
import vn.triumphstudio.elearning.domain.entity.ExamEntity;
import vn.triumphstudio.elearning.domain.entity.ExerciseEntity;
import vn.triumphstudio.elearning.domain.entity.PostCommentEntity;
import vn.triumphstudio.elearning.domain.entity.PostEntity;

import java.util.List;

public class PostInfo extends PostEntity {

    private int totalLike;

    private int totalUnlike;

    private List<PostCommentEntity> comments;

    private ExamEntity examInfo;

    private ExerciseEntity exerciseInfo;

    public PostInfo(PostEntity postEntity) {
        BeanUtils.copyProperties(postEntity, this);
        super.setCreatedAt(postEntity.getCreatedAt());
        super.setUpdatedAt(postEntity.getUpdatedAt());
    }

    public int getTotalLike() {
        return totalLike;
    }

    public void setTotalLike(int totalLike) {
        this.totalLike = totalLike;
    }

    public int getTotalUnlike() {
        return totalUnlike;
    }

    public void setTotalUnlike(int totalUnlike) {
        this.totalUnlike = totalUnlike;
    }

    public List<PostCommentEntity> getComments() {
        return comments;
    }

    public void setComments(List<PostCommentEntity> comments) {
        this.comments = comments;
    }

    public ExamEntity getExamInfo() {
        return examInfo;
    }

    public void setExamInfo(ExamEntity examInfo) {
        this.examInfo = examInfo;
    }

    public ExerciseEntity getExerciseInfo() {
        return exerciseInfo;
    }

    public void setExerciseInfo(ExerciseEntity exerciseInfo) {
        this.exerciseInfo = exerciseInfo;
    }
}
