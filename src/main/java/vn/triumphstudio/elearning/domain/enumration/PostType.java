package vn.triumphstudio.elearning.domain.enumration;

public enum PostType {
    Post, Exam, Exercise
}
