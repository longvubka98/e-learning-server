package vn.triumphstudio.elearning.domain.enumration;

public enum AuthProvider {
    local,
    facebook,
    google
}
