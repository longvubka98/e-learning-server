package vn.triumphstudio.elearning.domain.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExamRequest extends PostRequest {

    private String grade;

    private String subjects;

    private String semester;

    private int totalQuestions;

    private int duration;

    private List<AnswerRequest> answers;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public int getTotalQuestions() {
        return totalQuestions;
    }

    public void setTotalQuestions(int totalQuestions) {
        this.totalQuestions = totalQuestions;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public List<AnswerRequest> getAnswers() {
        return answers;
    }

    public void setAnswers(List<AnswerRequest> answers) {
        this.answers = answers;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AnswerRequest {
        private Integer questionIndex;

        private Integer answerIndex;

        public Integer getQuestionIndex() {
            return questionIndex;
        }

        public void setQuestionIndex(Integer questionIndex) {
            this.questionIndex = questionIndex;
        }

        public Integer getAnswerIndex() {
            return answerIndex;
        }

        public void setAnswerIndex(Integer answerIndex) {
            this.answerIndex = answerIndex;
        }
    }
}
