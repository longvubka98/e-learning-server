package vn.triumphstudio.elearning.domain.request;

import java.util.Date;
import java.util.List;

public class SubmitResultRequest {

    private Date startedAt;

    private List<ExamRequest.AnswerRequest> answers;

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public List<ExamRequest.AnswerRequest> getAnswers() {
        return answers;
    }

    public void setAnswers(List<ExamRequest.AnswerRequest> answers) {
        this.answers = answers;
    }
}
