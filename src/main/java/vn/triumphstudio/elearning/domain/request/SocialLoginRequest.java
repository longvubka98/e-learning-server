package vn.triumphstudio.elearning.domain.request;

import vn.triumphstudio.elearning.domain.enumration.AuthProvider;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class SocialLoginRequest {

    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthProvider provider;

    @NotEmpty
    private String token;

    private String expoPushNotificationKey;

    public AuthProvider getProvider() {
        return provider;
    }

    public void setProvider(AuthProvider provider) {
        this.provider = provider;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpoPushNotificationKey() {
        return expoPushNotificationKey;
    }

    public void setExpoPushNotificationKey(String expoPushNotificationKey) {
        this.expoPushNotificationKey = expoPushNotificationKey;
    }
}
