package vn.triumphstudio.elearning.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.triumphstudio.elearning.domain.request.SocialLoginRequest;
import vn.triumphstudio.elearning.domain.response.AuthResponse;
import vn.triumphstudio.elearning.service.AuthService;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService authService;

    @PostMapping("/login/social")
    public AuthResponse socialLoginUser(@Valid @RequestBody SocialLoginRequest request) {
        return authService.authenticateUser(request);
    }
}
