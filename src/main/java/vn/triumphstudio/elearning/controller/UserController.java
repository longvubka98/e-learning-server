package vn.triumphstudio.elearning.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vn.triumphstudio.elearning.domain.entity.ExamResultEntity;
import vn.triumphstudio.elearning.domain.entity.NotificationEntity;
import vn.triumphstudio.elearning.domain.entity.PostEntity;
import vn.triumphstudio.elearning.domain.entity.UserEntity;
import vn.triumphstudio.elearning.domain.enumration.VoteType;
import vn.triumphstudio.elearning.domain.request.*;
import vn.triumphstudio.elearning.domain.response.GroupInfo;
import vn.triumphstudio.elearning.domain.response.PostInfo;
import vn.triumphstudio.elearning.domain.response.RankingResponse;
import vn.triumphstudio.elearning.service.UserService;
import vn.triumphstudio.elearning.util.SecurityContextUtil;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @GetMapping("/me")
    private UserEntity getProfile() {
        return this.userService.getUserById(SecurityContextUtil.getCurrentUser().getId());
    }

    /**
     * Posts
     *
     * @return
     */
    @GetMapping("/me/posts")
    public List<PostInfo> posts() {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.getAllPosts(userId);
    }

    @PostMapping(value = "/me/posts", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PostEntity createPost(@RequestParam("post") String post, @RequestParam(value = "pictures", required = false) MultipartFile[] pictures) throws IOException {
        PostRequest request = this.objectMapper.readValue(post, PostRequest.class);
        request.setPictures(pictures);
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.createPost(userId, request);
    }

    @DeleteMapping("/me/posts/{id}")
    public void deletePost(@PathVariable long id) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        this.userService.deletePost(userId, id);
    }

    @GetMapping(value = "/posts")
    public Page<PostInfo> getAllPost(@RequestParam("page") int page, @RequestParam("size") int size) {
        return this.userService.getAllPosts(page, size);
    }

    @GetMapping(value = "/posts/{id}")
    public PostInfo getPostInfo(@PathVariable long id) {
        return this.userService.getPostInfo(id);
    }

    @PatchMapping(value = "/posts/{postId}/vote/{voteType}")
    public PostInfo votePost(@PathVariable long postId, @PathVariable("voteType") VoteType voteType) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.votePost(postId, userId, voteType);
    }

    @PostMapping(value = "/posts/{postId}/comment")
    public PostInfo commentPost(@PathVariable long postId, @RequestBody Map<String, Object> body) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.commentPost(postId, userId, (String) body.get("message"));
    }

    /**
     * GROUPS
     */
    @GetMapping(value = "/me/groups")
    public List<GroupInfo> getMyGroups() {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.getGroupsByUser(userId);
    }

    @PostMapping(value = "/me/groups")
    public GroupInfo createGroup(@RequestBody GroupRequest groupRequest) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.createGroup(userId, groupRequest);
    }

    @PatchMapping("/me/groups/{groupId}/join")
    public GroupInfo joinGroup(@PathVariable long groupId) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.joinGroup(userId, groupId);
    }

    @PostMapping(value = "/me/groups/{groupId}/posts", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public GroupInfo createGroupPost(@PathVariable long groupId, @RequestParam("post") String post, @RequestParam(value = "pictures", required = false) MultipartFile[] pictures) throws IOException {
        PostRequest request = this.objectMapper.readValue(post, PostRequest.class);
        request.setPictures(pictures);
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.createGroupPost(groupId, userId, request);
    }

    @GetMapping("/groups")
    public List<GroupInfo> listAllGroups() {
        return this.userService.getAllGroups();
    }

    @GetMapping("/groups/{id}")
    public GroupInfo getGroupInfo(@PathVariable long id) {
        return this.userService.getGroupInfo(id);
    }

    /**
     * Notifications
     *
     * @return
     */
    @GetMapping("/notifications/me")
    public List<NotificationEntity> getMyNotifications() {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.getNotificationsByUser(userId);
    }

    /**
     * Examination
     */
    @PostMapping(value = "/me/examinations", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PostInfo createExam(@RequestParam("exam") String exam, @RequestParam(value = "pictures", required = false) MultipartFile[] pictures) throws IOException {
        ExamRequest request = this.objectMapper.readValue(exam, ExamRequest.class);
        request.setPictures(pictures);
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.createExam(userId, request);
    }

    @PostMapping(value = "/me/examinations/{postId}/submit-result")
    public ExamResultEntity submitResult(@PathVariable long postId, @RequestBody SubmitResultRequest request) {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.submitResult(userId, postId, request);
    }

    @GetMapping(value = "/me/examinations/result")
    public List<ExamResultEntity> getMyResults() {
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.getMyExamResults(userId);
    }

    @GetMapping(value = "/examinations/ranking")
    public List<RankingResponse> getRanking() {
        return this.userService.getRanking();
    }

    /**
     * Exercise
     */
    @PostMapping(value = "/me/exercise", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_JSON_VALUE})
    public PostInfo createExercise(@RequestParam("exercise") String exercise, @RequestParam(value = "pictures", required = false) MultipartFile[] pictures) throws IOException {
        ExerciseRequest request = this.objectMapper.readValue(exercise, ExerciseRequest.class);
        request.setPictures(pictures);
        long userId = SecurityContextUtil.getCurrentUser().getId();
        return this.userService.createExercise(userId, request);
    }
}
