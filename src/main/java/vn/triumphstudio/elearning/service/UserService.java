package vn.triumphstudio.elearning.service;

import org.springframework.data.domain.Page;
import vn.triumphstudio.elearning.domain.entity.*;
import vn.triumphstudio.elearning.domain.enumration.VoteType;
import vn.triumphstudio.elearning.domain.request.*;
import vn.triumphstudio.elearning.domain.response.GroupInfo;
import vn.triumphstudio.elearning.domain.response.PostInfo;
import vn.triumphstudio.elearning.domain.response.RankingResponse;

import java.util.List;

public interface UserService {

    UserEntity getUserById(long id);

    UserEntity getUserByEmail(String email);

    PostEntity createPost(long userId, PostRequest postRequest);

    void deletePost(long userId, long postId);

    PostEntity getPost(long postId);

    List<PostInfo> getAllPosts(long userId);

    Page<PostInfo> getAllPosts(int page, int size);

    PostInfo getPostInfo(long postId);

    PostInfo votePost(long postId, long userId, VoteType voteType);

    PostInfo commentPost(long postId, long userId, String message);

    GroupInfo createGroup(long userId, GroupRequest groupRequest);

    GroupEntity getGroup(long groupId);

    GroupInfo joinGroup(long userId, long groupId);

    GroupInfo createGroupPost(long groupId, long userId, PostRequest postRequest);

    List<GroupInfo> getGroupsByUser(long userId);

    List<GroupInfo> getAllGroups();

    GroupInfo getGroupInfo(long id);

    List<NotificationEntity> getNotificationsByUser(long userId);

    PostInfo createExam(long userId, ExamRequest request);

    PostInfo createExercise(long userId, ExerciseRequest request);

    ExamResultEntity submitResult(long userId, long postId, SubmitResultRequest request);

    List<ExamResultEntity> getMyExamResults(long userId);

    List<RankingResponse> getRanking();
}
