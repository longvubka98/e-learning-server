package vn.triumphstudio.elearning.service;

import vn.triumphstudio.elearning.domain.dto.NotificationPayload;
import vn.triumphstudio.elearning.domain.entity.UserEntity;

public interface NotificationService {
    void sendNotification(NotificationPayload payload, UserEntity receiver, UserEntity sender);
}
