package vn.triumphstudio.elearning.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestOperations;
import vn.triumphstudio.elearning.domain.dto.NotificationPayload;
import vn.triumphstudio.elearning.domain.entity.NotificationEntity;
import vn.triumphstudio.elearning.domain.entity.UserEntity;
import vn.triumphstudio.elearning.property.AppProperties;
import vn.triumphstudio.elearning.repository.NotificationRepository;
import vn.triumphstudio.elearning.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

    @Autowired
    private RestOperations restOperations;

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private NotificationRepository notificationRepository;

    @Override
    public void sendNotification(NotificationPayload payload, UserEntity receiver, UserEntity sender) {
        NotificationEntity notification = new NotificationEntity();
        notification.setReceiver(receiver);
        notification.setSender(sender);
        notification.setMessage(payload.getBody());
        notification.setTitle(payload.getTitle());
        this.notificationRepository.save(notification);

        if (payload.getTo().isEmpty()) return;
        try {
            ResponseEntity<Object> result = restOperations.postForEntity(appProperties.getExpo().getPushNotificationEndPoint(), payload, Object.class);
            if (!result.getStatusCode().equals(HttpStatus.OK)) {
                LOGGER.error("Exponent Push Notification can't delivered message to [{}]", payload.getTo());
            }
        } catch (Exception e) {
            LOGGER.error("Exponent Push Notification can't delivered message to [{}]", payload.getTo());
        }
    }
}
