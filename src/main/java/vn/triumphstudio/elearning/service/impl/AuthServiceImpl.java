package vn.triumphstudio.elearning.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import vn.triumphstudio.elearning.domain.entity.UserEntity;
import vn.triumphstudio.elearning.domain.request.SocialLoginRequest;
import vn.triumphstudio.elearning.domain.response.AuthResponse;
import vn.triumphstudio.elearning.exception.BusinessLogicException;
import vn.triumphstudio.elearning.remote.OAuth2UserInfo;
import vn.triumphstudio.elearning.remote.SocialProviderAdapter;
import vn.triumphstudio.elearning.repository.UserRepository;
import vn.triumphstudio.elearning.security.JwtTokenProvider;
import vn.triumphstudio.elearning.security.UserPrincipal;
import vn.triumphstudio.elearning.service.AuthService;
import vn.triumphstudio.elearning.service.UserService;
import vn.triumphstudio.elearning.util.EmailUtils;

@Service
public class AuthServiceImpl implements AuthService {

    private final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Autowired
    private SocialProviderAdapter socialProviderAdapter;

    @Override
    public AuthResponse authenticateUser(SocialLoginRequest request) {
        LOGGER.info("Begin login social with on [{}]", request.getProvider().name());
        OAuth2UserInfo userInfo = socialProviderAdapter.getUserInfo(request);
        if (userInfo == null)
            throw new BusinessLogicException("User is null.");

        UserEntity userEntity = createOrUpdateUser(request, userInfo);
        if (!userEntity.isEnabled())
            throw new BusinessLogicException("Your account has been disabled.");

        UserPrincipal userPrincipal = new UserPrincipal(userEntity);
        AuthResponse response = new AuthResponse(jwtTokenProvider.generateAccessToken(userPrincipal));

        LOGGER.info("Finish authenticate User with email [{}]", userEntity.getEmail());
        return response;
    }

    private UserEntity createOrUpdateUser(SocialLoginRequest socialLoginRequest, OAuth2UserInfo oAuth2UserInfo) {
        String email = StringUtils.hasText(oAuth2UserInfo.getEmail()) ? oAuth2UserInfo.getEmail() : EmailUtils.generateDefaultEmail(socialLoginRequest.getProvider(), oAuth2UserInfo.getId());

        UserEntity userEntity = userRepository.findByEmail(email)
                .orElseGet(() -> createNewUser(email, socialLoginRequest, oAuth2UserInfo));

        return this.updateExistingUser(socialLoginRequest, userEntity, oAuth2UserInfo);
    }

    private UserEntity updateExistingUser(SocialLoginRequest socialLoginRequest, UserEntity existingUser, OAuth2UserInfo oAuth2UserInfo) {
        existingUser.setName(oAuth2UserInfo.getName());
        existingUser.setAvatarUrl(oAuth2UserInfo.getImageUrl());
        if (!StringUtils.isEmpty(socialLoginRequest.getExpoPushNotificationKey())) {
            existingUser.setExpoPushNotificationKey(socialLoginRequest.getExpoPushNotificationKey());
        }
        return userRepository.save(existingUser);
    }

    private UserEntity createNewUser(String email, SocialLoginRequest socialLoginRequest, OAuth2UserInfo oAuth2UserInfo) {
        UserEntity user = new UserEntity();

        user.setName(oAuth2UserInfo.getName());
        user.setEmail(email);
        user.setAvatarUrl(oAuth2UserInfo.getImageUrl());
        user.setProviderId(oAuth2UserInfo.getId());
        user.setEnabled(true);
        user.setExpoPushNotificationKey(StringUtils.isEmpty(socialLoginRequest.getExpoPushNotificationKey()) ? "" : socialLoginRequest.getExpoPushNotificationKey());
        user.setProviderType(socialLoginRequest.getProvider());

        return user;
    }
}
