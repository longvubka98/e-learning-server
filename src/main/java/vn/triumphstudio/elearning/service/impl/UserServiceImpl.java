package vn.triumphstudio.elearning.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.triumphstudio.elearning.domain.dto.NotificationPayload;
import vn.triumphstudio.elearning.domain.entity.*;
import vn.triumphstudio.elearning.domain.enumration.PostType;
import vn.triumphstudio.elearning.domain.enumration.VoteType;
import vn.triumphstudio.elearning.domain.request.*;
import vn.triumphstudio.elearning.domain.response.FileUploadResponse;
import vn.triumphstudio.elearning.domain.response.GroupInfo;
import vn.triumphstudio.elearning.domain.response.PostInfo;
import vn.triumphstudio.elearning.domain.response.RankingResponse;
import vn.triumphstudio.elearning.exception.BusinessLogicException;
import vn.triumphstudio.elearning.repository.*;
import vn.triumphstudio.elearning.service.FileStorageService;
import vn.triumphstudio.elearning.service.NotificationService;
import vn.triumphstudio.elearning.service.UserService;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private PostVoteRepository postVoteRepository;

    @Autowired
    private PostCommentRepository postCommentRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupMemberRepository groupMemberRepository;

    @Autowired
    private GroupPostRepository groupPostRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private NotificationRepository notificationRepository;

    @Autowired
    private ExamRepository examRepository;

    @Autowired
    private ExerciseRepository exerciseRepository;

    @Autowired
    private ExamResultRepository examResultRepository;

    @Override
    public UserEntity getUserById(long id) {
        return this.userRepository.findById(id).orElseThrow(() -> new BusinessLogicException("No user found with id: " + id));
    }

    @Override
    public UserEntity getUserByEmail(String email) {
        return this.userRepository.findByEmail(email).orElseThrow(() -> new BusinessLogicException("No user found with email: " + email));
    }

    @Override
    @Transactional
    public PostEntity createPost(long userId, PostRequest postRequest) {
        PostEntity postEntity = new PostEntity();

        if (postRequest.getPictures() != null) {
            List<PostImageEntity> images = new ArrayList<>();
            for (MultipartFile picture : postRequest.getPictures()) {
                FileUploadResponse uploaded = this.fileStorageService.uploadFile(picture);
                PostImageEntity image = new PostImageEntity();
                image.setPicture(uploaded.getFileName());
                image.setPost(postEntity);

                images.add(image);
            }
            postEntity.setImages(images);
        }
        postEntity.setMessage(postRequest.getMessage());
        postEntity.setPostType(postRequest.getPostType());
        postEntity.setUser(this.getUserById(userId));
        return this.postRepository.save(postEntity);
    }

    @Override
    @Transactional
    public void deletePost(long userId, long postId) {
        PostEntity postEntity = this.postRepository.findFirstByIdAndUser_Id(postId, userId);
        this.postRepository.delete(postEntity);
    }

    @Override
    public PostEntity getPost(long postId) {
        return this.postRepository.findById(postId).orElseThrow(() -> new BusinessLogicException("No post found with id = " + postId));
    }

    @Override
    public List<PostInfo> getAllPosts(long userId) {
        Sort sort = Sort.by("createdAt", "updatedAt").descending();
        return this.postRepository.findAllByUser_Id(userId, sort).stream().map(this::buildPostInfo).collect(Collectors.toList());
    }

    @Override
    public Page<PostInfo> getAllPosts(int page, int size) {
        Sort sort = Sort.by("createdAt", "updatedAt").descending();
        PageRequest pageRequest = PageRequest.of(page, size, sort);
        return this.postRepository.findAll(pageRequest).map(this::buildPostInfo);
    }

    @Override
    public PostInfo getPostInfo(long postId) {
        PostEntity postEntity = this.getPost(postId);
        return this.buildPostInfo(postEntity);
    }

    @Override
    @Transactional
    public PostInfo votePost(long postId, long userId, VoteType voteType) {
        PostEntity postEntity = this.getPost(postId);
        boolean isNew = false;

        PostVoteEntity postVoteEntity = this.postVoteRepository.findFirstByPost_IdAndUser_Id(postId, userId);
        if (postVoteEntity == null) {
            isNew = true;
            postVoteEntity = new PostVoteEntity();
            postVoteEntity.setPost(postEntity);
            postVoteEntity.setUser(this.getUserById(userId));
            postVoteEntity.setVoteType(voteType);
            this.postVoteRepository.save(postVoteEntity);
        } else {
            if (postVoteEntity.getVoteType().equals(voteType)) {
                this.postVoteRepository.delete(postVoteEntity);
            } else {
                postVoteEntity.setVoteType(voteType);
                this.postVoteRepository.save(postVoteEntity);
            }
        }
        if (isNew && !StringUtils.isEmpty(postEntity.getUser().getExpoPushNotificationKey())) {
            NotificationPayload payload = new NotificationPayload();
            payload.setTitle("New Vote");
            payload.setBody(String.format("%s was %s your post", this.getUserById(userId).getName(), voteType.name()));
            payload.setTo(postEntity.getUser().getExpoPushNotificationKey());
            new Thread(() -> notificationService.sendNotification(payload, postEntity.getUser(), this.getUserById(userId))).start();
        }
        return this.buildPostInfo(postEntity);
    }

    @Override
    @Transactional
    public PostInfo commentPost(long postId, long userId, String message) {
        PostEntity postEntity = this.getPost(postId);

        PostCommentEntity comment = new PostCommentEntity();
        comment.setPost(postEntity);
        comment.setUser(this.getUserById(userId));
        comment.setMessage(message);
        this.postCommentRepository.save(comment);

        if (!StringUtils.isEmpty(postEntity.getUser().getExpoPushNotificationKey())) {
            NotificationPayload payload = new NotificationPayload();
            payload.setTitle("New Comment");
            payload.setBody(String.format("%s was comment on your post", this.getUserById(userId).getName()));
            payload.setTo(postEntity.getUser().getExpoPushNotificationKey());
            new Thread(() -> notificationService.sendNotification(payload, postEntity.getUser(), this.getUserById(userId))).start();
        }

        return this.buildPostInfo(postEntity);
    }

    @Override
    @Transactional
    public GroupInfo createGroup(long userId, GroupRequest groupRequest) {
        GroupEntity newGroup = new GroupEntity();
        newGroup.setCreatedBy(this.getUserById(userId));
        newGroup.setName(groupRequest.getName());
        newGroup.setDescriptions(groupRequest.getDescriptions());

        this.groupRepository.saveAndFlush(newGroup);
        return this.joinGroup(userId, newGroup.getId());
    }

    @Override
    public GroupEntity getGroup(long groupId) {
        return this.groupRepository.findById(groupId).orElseThrow(() -> new BusinessLogicException("No group found with id = " + groupId));
    }

    @Override
    @Transactional
    public GroupInfo joinGroup(long userId, long groupId) {
        GroupMemberEntity existed = this.groupMemberRepository.findFirstByGroup_IdAndMember_Id(groupId, userId);
        if (existed != null) throw new BusinessLogicException("You have already joined this group");

        GroupMemberEntity groupMember = new GroupMemberEntity();
        groupMember.setGroup(this.getGroup(groupId));
        groupMember.setMember(this.getUserById(userId));
        this.groupMemberRepository.save(groupMember);

        return this.buildGroupInfo(this.getGroup(groupId));
    }

    @Override
    @Transactional
    public GroupInfo createGroupPost(long groupId, long userId, PostRequest postRequest) {
        GroupEntity group = this.getGroup(groupId);

        PostEntity postEntity = new PostEntity();

        if (postRequest.getPictures() != null) {
            List<PostImageEntity> images = new ArrayList<>();
            for (MultipartFile picture : postRequest.getPictures()) {
                FileUploadResponse uploaded = this.fileStorageService.uploadFile(picture);
                PostImageEntity image = new PostImageEntity();
                image.setPicture(uploaded.getFileName());
                image.setPost(postEntity);

                images.add(image);
            }
            postEntity.setImages(images);
        }
        postEntity.setGroup(group);
        postEntity.setMessage(postRequest.getMessage());
        postEntity.setPostType(postRequest.getPostType());
        postEntity.setUser(this.getUserById(userId));
        this.postRepository.save(postEntity);

        return this.buildGroupInfo(group);
    }

    @Override
    public List<GroupInfo> getGroupsByUser(long userId) {
        List<GroupMemberEntity> groupMemberEntities = this.groupMemberRepository.findAllByMember_Id(userId);
        return groupMemberEntities.stream().map(m -> this.buildGroupInfo(m.getGroup())).collect(Collectors.toList());
    }

    @Override
    public List<GroupInfo> getAllGroups() {
        return this.groupRepository.findAll(Sort.by("createdAt").descending()).stream().map(this::buildGroupInfo).collect(Collectors.toList());
    }

    @Override
    public GroupInfo getGroupInfo(long id) {
        GroupEntity group = this.getGroup(id);
        return this.buildGroupInfo(group);
    }

    @Override
    public List<NotificationEntity> getNotificationsByUser(long userId) {
        Sort sort = Sort.by("createdAt").descending();
        return this.notificationRepository.findAllByReceiver_Id(userId, sort);
    }

    @Override
    @Transactional
    public PostInfo createExam(long userId, ExamRequest request) {
        PostEntity postEntity = this.createPost(userId, request);

        ExamEntity exam = new ExamEntity();
        exam.setPost(postEntity);
        exam.setDuration(request.getDuration());
        exam.setGrade(request.getGrade());
        exam.setSubjects(request.getSubjects());
        exam.setSemester(request.getSemester());
        exam.setTotalQuestions(request.getTotalQuestions());

        List<AnswerEntity> answerEntities = new ArrayList<>();
        if (!request.getAnswers().isEmpty()) {
            for (ExamRequest.AnswerRequest rq : request.getAnswers()) {
                AnswerEntity answer = new AnswerEntity();
                answer.setExam(exam);
                answer.setAnswerIndex(rq.getAnswerIndex());
                answer.setQuestionIndex(rq.getQuestionIndex());

                answerEntities.add(answer);
            }
        }
        exam.setAnswers(answerEntities);
        this.examRepository.save(exam);

        return this.buildPostInfo(postEntity);
    }

    @Override
    @Transactional
    public PostInfo createExercise(long userId, ExerciseRequest request) {
        PostEntity postEntity = this.createPost(userId, request);

        ExerciseEntity exercise = new ExerciseEntity();
        exercise.setPost(postEntity);
        exercise.setGrade(request.getGrade());
        exercise.setSubjects(request.getSubjects());
        exercise.setSemester(request.getSemester());

        this.exerciseRepository.save(exercise);

        return this.buildPostInfo(postEntity);
    }

    @Override
    public ExamResultEntity submitResult(long userId, long postId, SubmitResultRequest request) {
        ExamEntity exam = this.examRepository.findFirstByPost_Id(postId);
        Map<Integer, Integer> resultMap = new HashMap<>();
        for (AnswerEntity answer : exam.getAnswers()) {
            resultMap.put(answer.getQuestionIndex(), answer.getAnswerIndex());
        }

        int totalCorrect = 0;
        int totalWrong;
        for (ExamRequest.AnswerRequest answer : request.getAnswers()) {
            if (answer.getQuestionIndex() == null || answer.getAnswerIndex() == null) continue;

            if (resultMap.containsKey(answer.getQuestionIndex())) {
                if (Objects.equals(resultMap.get(answer.getQuestionIndex()), answer.getAnswerIndex())) {
                    totalCorrect += 1;
                }
            }
        }
        totalWrong = exam.getTotalQuestions() - totalCorrect;


        ExamResultEntity examResultEntity = new ExamResultEntity();
        examResultEntity.setExam(exam);
        examResultEntity.setUser(this.getUserById(userId));
        examResultEntity.setStartedAt(LocalDateTime.ofInstant(request.getStartedAt().toInstant(), ZoneId.systemDefault()));
        examResultEntity.setTotalCorrect(totalCorrect);
        examResultEntity.setTotalWrong(totalWrong);

        return this.examResultRepository.save(examResultEntity);
    }

    @Override
    public List<ExamResultEntity> getMyExamResults(long userId) {
        return this.examResultRepository.findAllByUser_Id(userId);
    }

    @Override
    public List<RankingResponse> getRanking() {
        List<RankingProjection> projections = this.examResultRepository.findTopUser();
        List<RankingResponse> results = new ArrayList<>();
        for (RankingProjection projection : projections) {
            RankingResponse ranking = new RankingResponse();
            ranking.setUser(this.getUserById(projection.getUserId()));
            ranking.setTotalCorrect(projection.getTotal());

            results.add(ranking);
        }
        return results;
    }

    private GroupInfo buildGroupInfo(GroupEntity groupEntity) {
        GroupInfo info = new GroupInfo(groupEntity);
        info.setMembers(this.groupMemberRepository.findAllByGroup_Id(groupEntity.getId()));
        info.setPosts(this.groupPostRepository.findAllByGroup_Id(groupEntity.getId()).stream().map(this::buildPostInfo).collect(Collectors.toList()));
        return info;
    }

    private PostInfo buildPostInfo(PostEntity post) {
        PostInfo result = new PostInfo(post);
        result.setTotalLike(postVoteRepository.countByPost_IdAndVoteType(post.getId(), VoteType.like));
        result.setTotalUnlike(postVoteRepository.countByPost_IdAndVoteType(post.getId(), VoteType.unlike));
        result.setComments(this.postCommentRepository.findAllByPost_Id(post.getId()));
        if (post.getPostType().equals(PostType.Exam)) {
            result.setExamInfo(this.examRepository.findFirstByPost_Id(post.getId()));
        }
        if (post.getPostType().equals(PostType.Exercise)) {
            result.setExerciseInfo(this.exerciseRepository.findFirstByPost_Id(post.getId()));
        }
        return result;
    }
}
