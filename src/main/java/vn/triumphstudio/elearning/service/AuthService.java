package vn.triumphstudio.elearning.service;


import vn.triumphstudio.elearning.domain.request.SocialLoginRequest;
import vn.triumphstudio.elearning.domain.response.AuthResponse;

public interface AuthService {
    AuthResponse authenticateUser(SocialLoginRequest request);
}
