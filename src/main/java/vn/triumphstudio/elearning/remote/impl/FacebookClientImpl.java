package vn.triumphstudio.elearning.remote.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;
import vn.triumphstudio.elearning.domain.enumration.AuthProvider;
import vn.triumphstudio.elearning.exception.BusinessLogicException;
import vn.triumphstudio.elearning.remote.AbstractSocialProviderClient;
import vn.triumphstudio.elearning.remote.OAuth2UserInfo;

@Component
@Qualifier("facebookClient")
public class FacebookClientImpl extends AbstractSocialProviderClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(FacebookClientImpl.class);
    private final RestOperations restOperations;
    private final String userInfoUrl;

    public FacebookClientImpl(@Qualifier("restOperations") RestOperations restOperations,
                              ObjectMapper objectMapper,
                              @Value("${spring.security.oauth2.client.provider.facebook.userInfoUri}")
                                      String facebookUserInfoUri) {
        super(AuthProvider.facebook, objectMapper);
        this.restOperations = restOperations;
        this.userInfoUrl = UriComponentsBuilder.fromUriString(facebookUserInfoUri)
                .queryParam("access_token", "{token}")
                .build()
                .toUriString();
    }

    @Override
    public OAuth2UserInfo getUserInfo(String accessToken) {
        LOGGER.debug("Getting user info from Facebook");
        try {
            Object rawUserInfo = restOperations.getForObject(userInfoUrl, Object.class, accessToken);
            return convertResponseToUserInfo(rawUserInfo);
        } catch (RestClientException e) {
            throw new BusinessLogicException("Could not fetch User info from Facebook!");
        }
    }
}
