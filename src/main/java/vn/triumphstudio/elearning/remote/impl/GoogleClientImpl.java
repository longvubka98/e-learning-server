package vn.triumphstudio.elearning.remote.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import vn.triumphstudio.elearning.domain.enumration.AuthProvider;
import vn.triumphstudio.elearning.remote.AbstractSocialProviderClient;
import vn.triumphstudio.elearning.remote.OAuth2UserInfo;


@Component
@Qualifier(value = "googleClient")
public class GoogleClientImpl extends AbstractSocialProviderClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(GoogleClientImpl.class);
    private final RestOperations restOperations;
    private final String userInfoUrl;

    public GoogleClientImpl(RestOperations restOperations,
                            ObjectMapper objectMapper,
                            @Value("${spring.security.oauth2.client.provider.google.userInfoUri}")
                                    String googleUserInfoUri) {
        super(AuthProvider.google, objectMapper);
        this.restOperations = restOperations;
        this.userInfoUrl = googleUserInfoUri;
    }

    @Override
    public OAuth2UserInfo getUserInfo(String accessToken) {
        LOGGER.debug("Getting user info from Google");
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);
        HttpEntity<String> httpEntity = new HttpEntity<>(headers);
        ResponseEntity<Object> responseEntity = restOperations.exchange(userInfoUrl, HttpMethod.GET, httpEntity, Object.class);
        return convertResponseToUserInfo(responseEntity.getBody());
    }
}
