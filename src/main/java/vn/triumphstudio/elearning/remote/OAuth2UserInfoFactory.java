package vn.triumphstudio.elearning.remote;


import vn.triumphstudio.elearning.domain.enumration.AuthProvider;
import vn.triumphstudio.elearning.exception.BusinessLogicException;

import java.util.Map;

public class OAuth2UserInfoFactory {
    public static OAuth2UserInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        if (registrationId.equalsIgnoreCase(AuthProvider.google.toString())) {
            return new GoogleOAuth2UserInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(AuthProvider.facebook.toString())) {
            return new FacebookOAuth2UserInfo(attributes);
        } else {
            throw new BusinessLogicException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
