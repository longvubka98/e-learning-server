package vn.triumphstudio.elearning.remote;


public interface SocialProviderClient {
    OAuth2UserInfo getUserInfo(String accessToken);
}
