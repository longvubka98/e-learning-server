package vn.triumphstudio.elearning.remote;

import com.fasterxml.jackson.databind.ObjectMapper;
import vn.triumphstudio.elearning.domain.enumration.AuthProvider;

import java.util.Map;


public abstract class AbstractSocialProviderClient implements SocialProviderClient {
    private final AuthProvider authProvider;
    private final ObjectMapper mapper;

    protected AbstractSocialProviderClient(AuthProvider authProvider, ObjectMapper mapper) {
        this.authProvider = authProvider;
        this.mapper = mapper;
    }

    @SuppressWarnings("unchecked")
    protected OAuth2UserInfo convertResponseToUserInfo(Object response) {
        Map<String, Object> attributes = mapper.convertValue(response, Map.class);
        return OAuth2UserInfoFactory.getOAuth2UserInfo(authProvider.name(), attributes);
    }
}
