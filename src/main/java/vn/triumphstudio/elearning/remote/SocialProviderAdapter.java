package vn.triumphstudio.elearning.remote;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import vn.triumphstudio.elearning.domain.enumration.AuthProvider;
import vn.triumphstudio.elearning.domain.request.SocialLoginRequest;
import vn.triumphstudio.elearning.exception.BusinessLogicException;
import vn.triumphstudio.elearning.remote.impl.FacebookClientImpl;
import vn.triumphstudio.elearning.remote.impl.GoogleClientImpl;


@Component
public class SocialProviderAdapter {

    @Autowired
    @Qualifier(value = "facebookClient")
    private FacebookClientImpl facebookClient;

    @Autowired
    @Qualifier(value = "googleClient")
    private GoogleClientImpl googleClient;

    public OAuth2UserInfo getUserInfo(SocialLoginRequest request) {
        if (request.getProvider().equals(AuthProvider.facebook)) {
            return facebookClient.getUserInfo(request.getToken());
        } else if (request.getProvider().equals(AuthProvider.google)) {
            return googleClient.getUserInfo(request.getToken());
        } else {
            throw new BusinessLogicException("Sorry! Login with " + request.getProvider().name() + " is not supported yet.");
        }
    }
}
