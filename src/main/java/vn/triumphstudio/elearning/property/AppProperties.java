package vn.triumphstudio.elearning.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "app")
public class AppProperties {

    private final ExpoProperties expo = new ExpoProperties();

    public ExpoProperties getExpo() {
        return expo;
    }

    public static class ExpoProperties {
        private String pushNotificationEndPoint;

        public String getPushNotificationEndPoint() {
            return pushNotificationEndPoint;
        }

        public void setPushNotificationEndPoint(String pushNotificationEndPoint) {
            this.pushNotificationEndPoint = pushNotificationEndPoint;
        }
    }
}
